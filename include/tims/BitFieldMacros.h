#define BitRange(lsb, count) (lsb):(lsb+count)
#define BitRangeStart(bit_range) (1?bit_range)
/** BitRangeEnd is the bit after the last bit included (i.e. the returned bit is NOT included the range) */
#define BitRangeEnd(bit_range) (0?bit_range)

#define BitRangeMask(bit_range) ( (~0 << BitRangeStart(bit_range)) ^ (~0 << BitRangeEnd(bit_range)) )
#define BitRangeShift(bit_range) BitRangeStart(bit_range)
#define BitRangeValue(bit_range, value) (((value) & BitRangeMask(bit_range)) >> BitRangeShift(bit_range))
#define BitRangeSetValue(bit_range, container, value) (((container) & BitRangeMask(bit_range)) | (value << BitRangeShift(bit_range)) )

#define BIT_FIELD_GETTER(name, bit_range, register_type) \
        static inline register_type name(register_type container) { return BitRangeValue(bit_range, container); }
#define BIT_FIELD_SETTER(name, bit_range, register_type) \
        static inline register_type name(register_type container, register_type value) { return BitRangeSetValue(bit_range, container, value); }
