#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest/doctest.h>
#include <tims/BitFieldMacros.h>

TEST_CASE("Extracting Bitfields-Values via Macro BitRangeValue works on integers.") {
    CHECK(BitRangeMask(1:2) == 0b10);

    CHECK(BitRangeValue(1:2, 0b10) == 0b1);
    CHECK(BitRangeValue(2:5, 0b1111011) == 0b110);
    CHECK(BitRangeStart(BitRange(2, 3)) == 2);
    CHECK(BitRangeEnd(BitRange(2, 3)) == 5);

    CHECK(BitRangeValue(30:31, 0b01000000'00000000'00000000'00000000) == 1);
    CHECK(BitRangeValue(29:31, 0b01100000'00000000'00000000'00000000) == 0b11);
    CHECK(BitRangeValue(BitRange(29, 2), 0b01100000'00000000'00000000'00000000) == 0b11);
}

TEST_CASE("Setting Bitfields-Values via Macro BitRangeSetValue works on integers.") {
    CHECK(BitRangeSetValue(29:31, 0, 0b11) == 0b01100000'00000000'00000000'00000000);
    CHECK(BitRangeSetValue(BitRange(29, 2), 0, 0b11) == 0b01100000'00000000'00000000'00000000);
}